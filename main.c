#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#define LED_PORT    GPIOC
#define LED_PIN_BLU GPIO8
#define LED_PIN_GRN GPIO9
#define TIM_PSC_DIV 48000
#define SECONDS     0.25

volatile unsigned int i;

int main(void) {
    rcc_clock_setup_in_hsi_out_48mhz();
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_TIM3);

    // Set up the GPIO for the connected LEDs
    //  GPIO_MODE_AF: Set the GPIO mode to alternate function
    //  GPIO_PUPD_NONE: No pull-up/pull-down resistor
    gpio_mode_setup(LED_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, LED_PIN_BLU | LED_PIN_GRN);

    // Set the output options for:
    //   GPIO_OTYPE_PP: Push-pull (required for alternate function?)
    //   GPIO_OSPEED_HIGH: Highest output speed
    gpio_set_output_options(LED_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, LED_PIN_BLU | LED_PIN_GRN);

    // Set the Alternate Function mode for the GPIO pins to AF0 (alternate functio mode 0)
    gpio_set_af(LED_PORT, GPIO_AF0, LED_PIN_BLU | LED_PIN_GRN);

    // Reset the timer peripheral
    rcc_periph_reset_pulse(RST_TIM3);

    // Options Explained:
    //   TIM_CR1_CKD_CK_INT: No divider
    //   TIM_CR1_CMS_EDGE: Center alignment (half of prescaler for compare)
    //   TIM_CR1_DIR_UP: Count up
    timer_set_mode(TIM3, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_CENTER_1, TIM_CR1_DIR_UP);

    // Set the prescaler (count)
    timer_set_prescaler(TIM3, (rcc_apb1_frequency/TIM_PSC_DIV)/2*SECONDS);

    // Disable preloading values and set the timer to continuous
    // Note: Continuous mode automatically clears the necessary status register
    // flags (TIMx_SR)
    timer_disable_preload(TIM3);
    timer_continuous_mode(TIM3);

    // Set the period of the timer to the full value
    timer_set_period(TIM3, TIM_PSC_DIV);

    // Set the output compare mode for each channel
    //   - OC3 (output channel 3) is connected to GPIO8 (LED_PIN_BLU)
    //   - PWM mode 1 in upcounting mode keeps the output ACTIVE as long as the
    //     counter > compare value
    //   - PWM mode 2 in upcounting mode keeps the output INACTIVE as long as
    //     the counter < compare value
    //
    // timer_set_oc_output_high is the default value on reset
    timer_set_oc_mode(TIM3, TIM_OC3, TIM_OCM_PWM1);
    timer_set_oc_mode(TIM3, TIM_OC4, TIM_OCM_PWM2);

    // Set the output compare value to half the prescaler for both channels
    int tim_oc_ids[2] = { TIM_OC3, TIM_OC4 };

    for (i = 0; i < (sizeof(tim_oc_ids)/sizeof(tim_oc_ids[0])); ++i) {
        timer_set_oc_value(TIM3, tim_oc_ids[i], (TIM_PSC_DIV/2));
    }

    // Enable the timer comparator output to channels 3 and 4 (tied to GPIO8 and GPIO9)
    // Note: these cannot be OR'd together
    timer_enable_oc_output(TIM3, TIM_OC3);
    timer_enable_oc_output(TIM3, TIM_OC4);

    // Start (enable) the counter
    timer_enable_counter(TIM3);

    while (1) {
        ;
    }

    return 0;
}
